import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  const fighterImg = createElement({
    tagName: 'img',
    className: position,
    attributes: { src: fighter.source}
  });
  const fighterName = createElement({
    tagName: 'p',
    className: 'text',
  })
  fighterName.innerText = `Name - ${fighter.name}`

  const fighterHealth = createElement({
    tagName: 'p',
    className: 'text',
  })
  fighterHealth.innerText = `Health - ${fighter.health}`

  fighterElement.append(fighterImg)
  fighterElement.append(fighterName)
  fighterElement.append(fighterHealth)
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
