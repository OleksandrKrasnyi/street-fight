import { controls } from '../../constants/controls';
import { showWinnerModal } from './modal/winner';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const {PlayerOneAttack, PlayerOneBlock, PlayerTwoAttack, PlayerTwoBlock, PlayerOneCriticalHitCombination, PlayerTwoCriticalHitCombination} = controls;
    let firstFighterHealth = firstFighter.health;
    let secondFighterHealth = secondFighter.health;
    let fHealthIndicator = document.getElementById('left-fighter-indicator');
    let sHealthIndicator = document.getElementById('right-fighter-indicator');

    runOnKeys(
      () => {
        firstFighter =  {
          _id: firstFighter._id,
          name: firstFighter.name,
          health:firstFighter.health,
          attack : firstFighter.attack*2,
          defence:firstFighter.defence,
          source:firstFighter.source
        };
        const health = getDamage(getHitPower(firstFighter), 0);
        secondFighterHealth = secondFighterHealth - health;
        sHealthIndicator.style.width = `${(secondFighterHealth / firstFighter.health)*100}%`;
        win (firstFighter, secondFighter, firstFighterHealth, secondFighterHealth);
      },
        PlayerOneCriticalHitCombination[0],
        PlayerOneCriticalHitCombination[1],
        PlayerOneCriticalHitCombination[2]
    )

    runOnKeys(
      () => {
        secondFighter =  {
          _id: secondFighter._id,
          name: secondFighter.name,
          health:secondFighter.health,
          attack : secondFighter.attack*2,
          defence:secondFighter.defence,
          source:secondFighter.source
        };
        const health = getDamage(getHitPower(secondFighter), 0);
        firstFighterHealth = firstFighterHealth - health;
        fHealthIndicator.style.width = `${(firstFighterHealth / firstFighter.health)*100}%`;
        win (firstFighter, secondFighter, firstFighterHealth, secondFighterHealth);
      },
      PlayerTwoCriticalHitCombination[0],
      PlayerTwoCriticalHitCombination[1],
      PlayerTwoCriticalHitCombination[2]
    )

    runOnKeys(
      () => {
      const health = getDamage(getHitPower(firstFighter), getBlockPower(secondFighter));
      secondFighterHealth = secondFighterHealth - health;
      sHealthIndicator.style.width = `${(secondFighterHealth / firstFighter.health)*100}%`;
      win (firstFighter, secondFighter, firstFighterHealth, secondFighterHealth);
      },
      PlayerOneAttack,
      PlayerTwoBlock
    )

    runOnKeys(
      () => {
        const health = getDamage(getHitPower(secondFighter), getBlockPower(firstFighter));
        firstFighterHealth = firstFighterHealth - health;
        fHealthIndicator.style.width = `${(firstFighterHealth / firstFighter.health)*100}%`;
        win (firstFighter, secondFighter, firstFighterHealth, secondFighterHealth);
        },
      PlayerTwoAttack,
      PlayerOneBlock
    )
  });
}


export function getDamage(attacker, defender) {
  if(defender > attacker) {
    return 0
  }
  const damage = attacker - defender;
  return damage
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() +1;
  const attack  = fighter.attack;
  const power = attack * criticalHitChance;
  return power
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random()+1;
  const defence = fighter.defense;
  const power = defence * dodgeChance;
  return power
}

function runOnKeys(func, ...codes) {
  let pressed = new Set();

  document.addEventListener('keydown', function(event) {
    pressed.add(event.code);

    for (let code of codes) {
      if (!pressed.has(code)) {
        return;
      }
    }
    pressed.clear();

    func();
  });

  document.addEventListener('keyup', function(event) {
    pressed.delete(event.code);
  });

}

function win (firstFighter, secondFighter, firstFighterHealth, secondFighterHealth) {
    if (secondFighterHealth <= 0) {
      showWinnerModal(firstFighter);
       return firstFighter

     }
     if (firstFighterHealth <= 0) {
      showWinnerModal(secondFighter);
       return secondFighter
     }
}