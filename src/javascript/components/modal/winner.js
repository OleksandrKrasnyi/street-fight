import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview'

export  function showWinnerModal(fighter) {
  const elem = createFighterImage(fighter)
   showModal({
     title:`Winner ${fighter.name}`,
     bodyElement: elem, 
     onClose: () => {
      location = location
    }
    }) 
}
